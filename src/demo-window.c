
#include "demo-config.h"
#include "demo-window.h"
#include "demo-something.h"

struct _DemoWindow
{
  GtkApplicationWindow  parent_instance;
};

G_DEFINE_TYPE (DemoWindow, demo_window, GTK_TYPE_APPLICATION_WINDOW)

static void
demo_window_class_init (DemoWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/Demo/demo-window.ui");
}

static void
demo_window_init (DemoWindow *self)
{
//  g_type_ensure (DEMO_TYPE_SOMETHING);
  gtk_widget_init_template (GTK_WIDGET (self));
}

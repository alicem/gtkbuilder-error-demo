#include "demo-something.h"

struct _DemoSomething
{
  GtkWidget parent_instance;
};

G_DEFINE_TYPE (DemoSomething, demo_something, GTK_TYPE_WIDGET)

static void
demo_something_class_init (DemoSomethingClass *klass)
{
}

static void
demo_something_init (DemoSomething *self)
{
  gtk_widget_set_has_window (GTK_WIDGET (self), FALSE);
}
